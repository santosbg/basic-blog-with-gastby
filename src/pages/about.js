import React from 'react';
import { Link } from 'gatsby';

import Layout from '../components/Layout';

const AboutPage = () => {
  return (
    <Layout>
      <h1>About</h1>
      <p>Some stuff in the about page</p>
      <p>
        <Link to="/blog">Checkout my blog</Link>
      </p>
    </Layout>
  );
};

export default AboutPage;