import React from 'react';
import { Link, graphql, useStaticQuery } from 'gatsby';
import Layout from '../components/Layout';
import { posts, post } from './blog.module.scss';

const BlogPage = () => {
  const data = useStaticQuery(graphql`
    query {
      allMarkdownRemark {
        edges {
          node {
            frontmatter {
              title
              date
            }
            fields {
              slug
            }
          }
        }
      }
    }
  `);

  const renderBlogsPosts = () => {
    return data.allMarkdownRemark.edges.map(edge => {
      return (
        <li key={edge.node.frontmatter.title} className={post}>
          <Link to={`/blog/${edge.node.fields.slug}`}>
            <h2>{edge.node.frontmatter.title}</h2>
            <p>{edge.node.frontmatter.date}</p>
          </Link>
        </li>
      );
    });
  };

  return (
    <Layout>
      <h1>My Blog</h1>
      <ol className={posts}>
        {renderBlogsPosts()}
      </ol>
    </Layout>
  );
};

export default BlogPage;