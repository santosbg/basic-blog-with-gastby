import React from 'react';
import { Link } from 'gatsby';

import Layout from '../components/Layout';

const IndexPage = () => {
  return (
    <Layout>
      <h1>Hello,</h1>
      <h2>I'm Anton, a full-stack developer living in beautiful Bulgarian</h2>
      <p>Need a developer 
        <Link to="/contact">Contact me.</Link>
      </p>
      <p>
        <Link to="/about">See more about me </Link>
      </p>
    </Layout>
  );
};

export default IndexPage;