import React from 'react';

import Layout from '../components/Layout';

const ContactPage = () => {
  return (
    <Layout>
      <h1>
        Contact me on
      </h1>
      <p>
        Email: anton.n.ivanov8@gmail.com
      </p>
      <p>
        <a href="https://www.facebook.com/">or in Facebook</a>
      </p>
    </Layout>
  );
};

export default ContactPage;