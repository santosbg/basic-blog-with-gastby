import React from 'react';

import { footer } from './footer.module.scss';

const Footer = () => {
  return (
    <footer className={footer}>
      <p>Created by Anton Ivanov, @2020</p>
    </footer>
  );
};

export default Footer;